
#include "ServerThread.h"
#include "EventLoop.h"
#include "TcpServer.h"

using namespace muduo;

ServerThread::ServerThread(const std::string& threadName, 
                           const InetAddress& listenAddr, 
                           TcpServer* pTcpServer)
    : condition_(mutex_)
{
    started_ = false;
    joined_ = false;
    detached_ = false;
    name_ = threadName;
    listenAddr_ = listenAddr;
    pEventLoop_ = NULL;
    pTcpServer_ = pTcpServer;
}

ServerThread::~ServerThread()
{
    if (pEventLoop_ != NULL) 
    {
        // still a tiny chance to call destructed object, 
        // if threadFunc exits just now.but when EventLoopThread destructs, 
        // usually programming is exiting anyway.
        pEventLoop_->quit();
        
        if (started_ && (!joined_) && (!detached_))
        {
            ::pthread_detach(pthreadId_);
        }
    }
}

EventLoop* ServerThread::startThread()
{
    started_ = true;
    if (pthread_create(&pthreadId_, NULL, &threadEntry, this))
    {
        started_ = false;
        LOG_FATAL("pthread_create failed!");
    }

    // 等待线程启动运行后，获取线程的EventLoop对象指针
    {
        MutexLocking lock(mutex_);
        while (pEventLoop_ == NULL)
        {
            condition_.wait();
        }
    }

    return pEventLoop_;
}

void* ServerThread::threadEntry(void* arg)
{
    ServerThread* pServerThread = (ServerThread*)arg;
    
    // 在线程栈上创建线程自己的TcpServer和EventLoop对象
    EventLoop tcpServerLoop; 
    TcpServer tcpServer(0, pServerThread->pTcpServer_->listenAddress(),
                        pServerThread->pTcpServer_->name(), 
                        true, &tcpServerLoop);
    
    // EventLoop对象准备好以后，通知主线程获取对象指针
    {
        MutexLocking lock(pServerThread->mutex_);
        pServerThread->pEventLoop_ = &tcpServerLoop;
        pServerThread->condition_.notify();
    }

    tcpServer.start();
    
    tcpServerLoop.loop();
    
    pServerThread->pEventLoop_ = NULL;
    
    return NULL;
}

int ServerThread::join()
{
  int ret = 0;
  if((!joined_) && (!detached_) && started_)
  {
      joined_ = true;
      ret =  ::pthread_join(pthreadId_, NULL);
  }
  
  return ret;
}

int ServerThread::detach()
{
  int ret = 0;
  if((!detached_) && started_)
  {
      detached_ = true;
      ret =  ::pthread_detach(pthreadId_);
  }
  
  return ret;
}

