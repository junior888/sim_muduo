
#include "TcpLink.h"
#include "LinkOwner.h"
#include "EventLoop.h"
#include "Logging.h"
#include "Socket.h"

using namespace muduo;

TcpLink::TcpLink(LinkOwner* owner, 
                 const std::string& name,
                 int sockfd,
                 const InetAddress& localAddr,
                 const InetAddress& peerAddr)
    : FdEvent(owner->getLoop(), sockfd),
      state_(kConnected),
      owner_(owner),
      name_(name),
      localAddr_(localAddr),
      peerAddr_(peerAddr),
      context_(NULL)
{
    
}

TcpLink::~TcpLink()
{
    sockets::close(fd_);
    //assert(state_ == kDisconnected);
}

void TcpLink::send(const StringPiece& message)
{
    if (state_ == kConnected)
    {
        if (pEventLoop_->isInLoopThread())
        {
            sendInLoop(message);
        }
        else
        {
            pEventLoop_->runInLoop(std::bind(&bindSendInLoop,
                                   this,  message.as_string()));
        }
    }
}

void TcpLink::send(Buffer* buf)
{
    if (state_ == kConnected)
    {
        if (pEventLoop_->isInLoopThread())
        {
            sendInLoop(buf->peek(), buf->readableBytes());
            buf->retrieveAll();
        }
        else
        {
            pEventLoop_->runInLoop(std::bind(&bindSendInLoop,
                                   this, buf->retrieveAllAsString()));
        }
    }
}

void TcpLink::bindSendInLoop(TcpLink* conn, const std::string& message)
{
    conn->sendInLoop(message.data(), message.size());
}

void TcpLink::sendInLoop(const StringPiece& message)
{
    sendInLoop(message.data(), message.size());
}

void TcpLink::sendInLoop(const void* data, size_t len)
{
    pEventLoop_->assertInLoopThread();
    ssize_t nwrote = 0;
    size_t remaining = len;
    bool faultError = false;
    if (state_ == kDisconnected)
    {
        LOG_WARN("disconnected, give up writing");
        return;
    }
    // if no thing in output queue, try writing directly
    if (!isWriting() && outputBuffer_.readableBytes() == 0)
    {
        nwrote = ::write(fd_, data, len);
        if (nwrote >= 0)
        {
            remaining = len - nwrote;
            if (remaining == 0)
            {
                owner_->writeComplete(shared_from_this());
            }
        }
        else // nwrote < 0
        {
            nwrote = 0;
            if (errno != EWOULDBLOCK)
            {
                if (errno == EPIPE || errno == ECONNRESET) // FIXME: any others?
                {
                    faultError = true;
                }
            }
        }
    }

    assert(remaining <= len);
    if (!faultError && remaining > 0)
    {
        size_t oldLen = outputBuffer_.readableBytes();
        if (oldLen + remaining >= highWaterMark_
            && oldLen < highWaterMark_)
        {
            owner_->highWaterMark(shared_from_this(), oldLen + remaining);
        }
        outputBuffer_.append(static_cast<const char*>(data)+nwrote, remaining);
        if (!isWriting())
        {
            enableWriting(ModEvent);
        }
    }
}

void TcpLink::shutdown()
{
    if (state_ == kConnected)
    {
        setState(kDisconnecting);
        pEventLoop_->runInLoop(std::bind(&TcpLink::shutdownInLoop, this));
    }
}

void TcpLink::shutdownInLoop()
{
    pEventLoop_->assertInLoopThread();
    
    if (!isWriting())
    {
        sockets::shutdownWrite(fd_);
    }
}

void TcpLink::forceClose()
{
    if (state_ == kConnected)
    {
        setState(kDisconnecting);
        pEventLoop_->queueInLoop(std::bind(&TcpLink::forceCloseInLoop, shared_from_this()));
    }
}

void TcpLink::forceCloseInLoop()
{
    pEventLoop_->assertInLoopThread();
    if (state_ == kConnected)
    {
        handleClose();
    }
}

//void TcpLink::forceCloseWithDelay(double seconds)
//{
//    if (state_ == kConnected)
//    {
//        setState(kDisconnecting);

//        pEventLoop_->runAfter(seconds, 
//                 makeWeakCallback(shared_from_this(), &TcpLink::forceClose));  
//    }
//}

void TcpLink::handleRead(Timestamp& receiveTime)
{
    pEventLoop_->assertInLoopThread();
    int savedErrno = 0;
    ssize_t n = inputBuffer_.readFd(fd_, &savedErrno);
    if (n > 0)
    {
        // 通知客户端或服务器接收新的消息
        owner_->rcvMessage(shared_from_this(), &inputBuffer_, receiveTime);
    }
    else if (n == 0)
    {
        handleClose();
    }
    else
    {
        errno = savedErrno;
        handleError();
    }
}

void TcpLink::handleWrite()
{
    pEventLoop_->assertInLoopThread();
    if (isWriting())
    {
        ssize_t n = ::write(fd_, outputBuffer_.peek(), outputBuffer_.readableBytes());
        if (n > 0)
        {
            outputBuffer_.retrieve(n);
            if (outputBuffer_.readableBytes() == 0)
            {
                disableWriting(ModEvent);

                owner_->writeComplete(shared_from_this());
                
                if (state_ == kDisconnecting)
                {
                    shutdownInLoop();
                }
            }
        }
        else
        {
            LOG_ERROR("TcpConnection::handleWrite");
        }
    }
    else
    {
        LOG_TRACE("Connection fd=%d no more writing", fd_);
    }
}

void TcpLink::handleClose()
{
    pEventLoop_->assertInLoopThread();
    assert(state_ == kConnected || state_ == kDisconnecting);
    setState(kDisconnected);
    
    TcpLinkSPtr tcpLinkSPtr(shared_from_this());
    owner_->delConnection(tcpLinkSPtr);
}

void TcpLink::handleError()
{
    int err = sockets::getSocketError(fd_);
    LOG_ERROR("TcpLink handleError = %s", strerror(err));
}

