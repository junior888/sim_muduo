
#include "TcpServer.h"
#include "EventLoop.h"
#include "Logging.h"
#include "Socket.h"

#include <string> 
#include <stdio.h>  // snprintf
#include <functional>

using namespace muduo;

namespace muduo
{

// 新建连接回调函数
NewConnectionCallback TcpServer::newConnectionCallback_; 

// 关闭连接回调函数
DelConnectionCallback TcpServer::delConnectionCallback_; 

// 接收到新消息回调函数
RcvMessageCallback    TcpServer::rcvMessageCallback_; 

// 发送缓冲区已清空回调函数
WriteCompleteCallback TcpServer::writeCompleteCallback_; 

// 发送缓冲区已到达高水线回调函数
HighWaterMarkCallback TcpServer::highWaterMarkCallback_; 

}

TcpServer::TcpServer(unsigned int numEventThreads,
                     const InetAddress& listenAddr,
                     const std::string& nameArg,
                     bool  isReusePort, EventLoop* pMainLoop)
    : pMainLoop_(pMainLoop),
      listenAddress_(listenAddr),
      name_(nameArg),
      numEventThreads_(numEventThreads),
      nextLinkId_(1)
{
    if (pMainLoop != NULL)
    {
        listenSocket_ = make_unique<ListenSocket>(this, listenAddr, true);
    }
}

TcpServer::~TcpServer()
{
    // 需要考虑拆除服务器所有的连接
    LOG_TRACE("TcpServer[%s] destructing", name_.c_str());
}


void TcpServer::start()
{
    if (started_.getAndSet(1) == 0)
    {
        startThreadPool();
        
        if (listenSocket_)
        {
            pMainLoop_->runInLoop(
                std::bind(&ListenSocket::startListen, listenSocket_.get()));
        }
    }
}


void TcpServer::startThreadPool()
{
    for (int i = 0; i < numEventThreads_; ++i)
    {
        char buf[32]={0};
        snprintf(buf, sizeof buf, "EventThread_%d", i);

        std::unique_ptr<ServerThread> serverThreadPtr(new ServerThread(buf, listenAddress_, this));
        EventLoop* tmpLoop = serverThreadPtr->startThread();

        serverThreadVect_.push_back(std::move(serverThreadPtr));
        ServerLoopVect_.push_back(tmpLoop);
    }
}

void TcpServer::joinThread()
{
    for (auto & x : serverThreadVect_)
    {
        x->join();
    }
}

void TcpServer::detachThread()
{
    for (auto & x : serverThreadVect_)
    {
        x->detach();
    }
}

void TcpServer::newConnection(int sockfd, const InetAddress& peerAddr)
{
    char buf[64]={0};
    snprintf(buf, sizeof buf, "%s#%d", 
             listenAddress_.toIpPort().c_str(), nextLinkId_);
    ++nextLinkId_;    
    std::string linkName(buf);
    
    InetAddress localAddr(sockets::getLocalAddr(sockfd));

    LOG_INFO("thread=%d new link fd=%d name=%s from %s", 
             pMainLoop_->threadId(), sockfd, linkName.c_str(), 
             peerAddr.toIpPort().c_str());

    TcpLinkSPtr tcpLinkSPtr= std::make_shared<TcpLink>(this, linkName, 
                                              sockfd, localAddr, peerAddr);
    
    tcpLinkMap_[linkName] = tcpLinkSPtr;

    tcpLinkSPtr->enableReading(AddEvent);

    if (newConnectionCallback_)
    {
        newConnectionCallback_(tcpLinkSPtr);
    } 
}


void TcpServer::delConnection(TcpLinkSPtr& conn)
{
    LOG_INFO("thread=%d del link fd=%d name=%s from %s",
              pMainLoop_->threadId(), conn->fd(), 
              conn->name().c_str(), conn->peerAddr().toIpPort().c_str());
              
    if (delConnectionCallback_)
    {
        delConnectionCallback_(conn);
    }  
    
    conn->updateFdEvent(DelEvent);
    
    // 从map容器中删除TcpLink的智能指针，
    // 正常情况下TcpLink对象也会被释放
    tcpLinkMap_.erase(conn->name());
}

void TcpServer::rcvMessage(const TcpLinkSPtr& conn, 
                           Buffer* buf, Timestamp time)
{
    if (rcvMessageCallback_)
    {
        rcvMessageCallback_(conn, buf, time);
    }
}

void TcpServer::writeComplete(const TcpLinkSPtr& conn)
{
    if (writeCompleteCallback_)
    {
        writeCompleteCallback_(conn);
    }
}

void TcpServer::highWaterMark(const TcpLinkSPtr& conn, size_t highMark)
{
    if (highWaterMarkCallback_)
    {
        highWaterMarkCallback_(conn, highMark);
    }
}

