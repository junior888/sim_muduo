#ifndef _MUDUO_SERVER_THREAD_H_
#define _MUDUO_SERVER_THREAD_H_

#include "copyable.h"
#include "ThreadUtil.h"
#include "InetAddress.h"

#include <string>
#include <functional>
#include <pthread.h>

namespace muduo
{
class EventLoop;
class TcpServer;

class ServerThread : noncopyable
{
public:

    ServerThread(const std::string& threadName, 
                 const InetAddress& listenAddr, TcpServer* pTcpServer);

    ~ServerThread();

    EventLoop* startThread();

    static void* threadEntry(void* arg);

    int join();
    
    int detach();
private:

    std::string name_;       // 线程名
    InetAddress listenAddr_; // 线程的监听地址
    
    pthread_t pthreadId_;    // 线程ID
    MutexLock mutex_;        // 
    Condition condition_;    // 

    bool      started_;
    bool      joined_;
    bool      detached_;
    
    // 每个EventLoop在构造的时候会记录自己所属的线程ID
    // 所以这里是一个指针，等到线程运行后再创建对象
    EventLoop* pEventLoop_;  

    TcpServer* pTcpServer_;
                             
};
}

#endif  // _MUDUO_SERVER_THREAD_H_
