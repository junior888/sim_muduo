#ifndef _MUDUO_TCPCLIENT_H_
#define _MUDUO_TCPCLIENT_H_

#include "LinkOwner.h"
#include "EventLoop.h"
#include "ConnectSocket.h"

#include <string>

namespace muduo
{

class TcpClient : public LinkOwner
{
public:

    TcpClient(const InetAddress& serverAddr, 
              const std::string& nameArg, EventLoop* pMainLoop);

    ~TcpClient();  

    void connect();

    void disconnect();

    void stop();

    void enableRetry() { retry_ = true; }

    bool retry() const { return retry_; }

    virtual EventLoop* getLoop()  const { return pMainLoop_; }

    virtual void newConnection(int sockfd, const InetAddress& peerAddr);

    virtual void delConnection(TcpLinkSPtr& conn);
    
    virtual void rcvMessage(const TcpLinkSPtr& conn, Buffer* buf, Timestamp time);

    virtual void writeComplete(const TcpLinkSPtr& conn);

    virtual void highWaterMark(const TcpLinkSPtr& conn, size_t highMark);

    static NewConnectionCallback newConnectionCallback_; // 新建连接回调函数
    
    static DelConnectionCallback delConnectionCallback_; // 关闭连接回调函数
    
    static RcvMessageCallback    rcvMessageCallback_;    // 接收到新消息回调函数

    static WriteCompleteCallback writeCompleteCallback_; // 消息发送完成回调函数

    static HighWaterMarkCallback highWaterMarkCallback_; // 
    
private:

    EventLoop* pMainLoop_;     // TcpClient所在线程的EventLoop

    InetAddress serverAddr_;   // 需要连接的目标服务器地址

    const std::string name_;   // 客户端名字

    bool retry_;               // 客户端连接被动断开后是否需要持续尝试连接

    bool connect_;             // 记录客户端是否主动断开

    mutable MutexLock mutex_;
    
    ConnectSocketUPtr connectSocket_; // 客户端连接套接字

    int nextLinkId_;          // 每个连接需要不同的ID作为名字

    TcpLinkSPtr tcpLinkSPtr_;  // 记录连接对象的(智能)指针
};

}
#endif  // _MUDUO_TCPCLIENT_H_
