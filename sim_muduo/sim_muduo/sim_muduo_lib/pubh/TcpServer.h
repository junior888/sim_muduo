
#ifndef _MUDUO_TCPSERVER_H_
#define _MUDUO_TCPSERVER_H_

#include "LinkOwner.h"
#include "ListenSocket.h"
#include "InetAddress.h"
#include "TcpLink.h"
#include "Atomic.h"
#include "EventLoop.h"
#include "ServerThread.h"

#include <memory>
#include <string>
#include <map>

namespace muduo
{

///
/// TCP server
///
class TcpServer : public LinkOwner
{
public:

    /// numEventThreads:需要创建的IO线程数量
    /// 如果numEventThreads = 0, 则要求pMainLoop != NULL
    /// 如果numEventThreads > 0, 则启动n个线程并用多个套接字并发监听同一个端口
    TcpServer(unsigned int numEventThreads, const InetAddress& listenAddr, 
              const std::string& nameArg, bool isReusePort, 
              EventLoop* pMainLoop);
            
    ~TcpServer();  
    
    void start();// 服务器启动函数

    void joinThread();

    void detachThread();
    
    const InetAddress& listenAddress() const { return listenAddress_; }
    
    const std::string& name() const { return name_; }

    /// 虚接口用于接收TcpLink建立或删除的通知
    virtual void newConnection(int sockfd, const InetAddress& peerAddr);

    virtual void delConnection(TcpLinkSPtr& conn);

    virtual void rcvMessage(const TcpLinkSPtr& conn, Buffer* buf, Timestamp time);

    virtual void writeComplete(const TcpLinkSPtr& conn);

    virtual void highWaterMark(const TcpLinkSPtr& conn, size_t highMark);
    
    virtual EventLoop* getLoop() const { return pMainLoop_; }
    
    // 新建连接回调函数
    static NewConnectionCallback newConnectionCallback_; 

    // 关闭连接回调函数
    static DelConnectionCallback delConnectionCallback_; 

    // 接收到新消息回调函数
    static RcvMessageCallback    rcvMessageCallback_;    

    // 发送缓冲区已清空回调函数
    static WriteCompleteCallback writeCompleteCallback_; 

    // 发送缓冲区已到达高水线回调函数
    static HighWaterMarkCallback highWaterMarkCallback_;

private:

    /// 启动IO线程池
    void startThreadPool(); 

private:

    EventLoop*  pMainLoop_;     // TcpServer所在主线程的EventLoop
    
    InetAddress listenAddress_; // server监听地址
    
    const std::string name_;    // 服务器名称
    
    AtomicInt32 started_;       // server启动标志
    
    const int numEventThreads_; // 需要额外创建的IO线程数量
    
    ///
    /// 每个EventThread对象对应一个IO线程
    /// 每个IO线程的栈上都有一个EventLoop对象
    /// EventThread对象通过智能指针管理，EventLoop对象通过线程栈管理
    /// 所以两者都不需要人工干预
    std::vector<std::unique_ptr<ServerThread> > serverThreadVect_;
    std::vector<EventLoop*> ServerLoopVect_;

    /// 每个线程都的监听套接字
    std::unique_ptr<ListenSocket> listenSocket_; 
    
    /// 已建立的TCP连接集合
    typedef std::map<std::string, TcpLinkSPtr> TcpLinkMap;
    
    int nextLinkId_;         // 每个连接需要不同的ID作为名字
    
    TcpLinkMap tcpLinkMap_;  // 以连接名为key，记录连接对象的(智能)指针
    
};

}

#endif  // _MUDUO_TCPSERVER_H_

