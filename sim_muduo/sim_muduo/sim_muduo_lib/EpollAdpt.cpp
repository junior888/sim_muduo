
#include "EpollAdpt.h"
#include "FdEvent.h"

#include <string.h>


using namespace muduo;

static_assert(EPOLLIN == POLLIN, "EPOLLIN");
static_assert(EPOLLPRI == POLLPRI, "EPOLLPRI");
static_assert(EPOLLOUT == POLLOUT, "EPOLLOUT");
static_assert(EPOLLRDHUP == POLLRDHUP, "EPOLLRDHUP");
static_assert(EPOLLERR == POLLERR, "EPOLLERR");
static_assert(EPOLLHUP == POLLHUP, "EPOLLHUPEPOLLHUP");


EpollAdpt::EpollAdpt(EventLoop* loop)
    :Poller(loop)
{
    events_.resize(16); // 初始化容器的大小，并且创建对象
    epollfd_ = ::epoll_create1(EPOLL_CLOEXEC);
    if (epollfd_ < 0)
    {
        LOG_FATAL("EpollAdpt failed");
    }
}

EpollAdpt::~EpollAdpt()
{
    ::close(epollfd_);
}

///
/// 调用系统的epoll_wait函数，得到处于就绪状态的描述符，
/// 并将对应的Channel对象保存到vector容器ChannelList中
/// 
Timestamp EpollAdpt::polling(int timeoutMs, FdEventList* pActFdEvents)
{
    int numEvents = ::epoll_wait(epollfd_, &*events_.begin(),
                                 static_cast<int>(events_.size()), timeoutMs);
    int savedErrno = errno;
    Timestamp now(Timestamp::now());
    if (numEvents > 0)
    {
        LOG_TRACE("%d events happended", numEvents);
        fillActiveFdEvents(numEvents, pActFdEvents);
        
        /// 和poll函数不同，epoll_wait返回的是当前处于活动状态的描述符数组
        /// 所以，这里判断如果events_容器(数组)空间已充满，就再扩大一倍
        if (size_t(numEvents) == events_.size())
        {
            events_.resize(events_.size()*2);
        }
    }
    else if (numEvents == 0)
    {
        LOG_TRACE("nothing happended");
    }
    else
    {
        if (savedErrno != EINTR)
        {
            errno = savedErrno;
            LOG_ERROR("Epolling errno=%", savedErrno);
        }
    }
    return now;
}

void EpollAdpt::fillActiveFdEvents(int numEvents, FdEventList* pActFdEvents) const
{
    assert(size_t(numEvents) <= events_.size());
    for (int i = 0; i < numEvents; ++i)
    {
        FdEvent* pFdEvent = static_cast<FdEvent*>(events_[i].data.ptr);
        
#ifndef NDEBUG
        int fd = pFdEvent->fd();
        FdEventMap::const_iterator it = fdEventMap_.find(fd);
        assert(it != fdEventMap_.end());
        assert(it->second == pFdEvent);
#endif
        // 记录epoll返回的描述符状态
        pFdEvent->set_revents(events_[i].events);
        pActFdEvents->push_back(pFdEvent);
    }
}

void EpollAdpt::addFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int inputEvent = pFdEvent->events();

    struct epoll_event event;
    bzero(&event, sizeof event);
    event.events = inputEvent;
    event.data.ptr = pFdEvent;
    
    if (::epoll_ctl(epollfd_, EPOLL_CTL_ADD, inputFd, &event) < 0)
    {
        LOG_FATAL("add fd = %d events = %d fail", inputFd, inputEvent);
    }
    else
    {
        fdEventMap_[inputFd] = pFdEvent;
        LOG_TRACE("add fd = %d; events = %d", inputFd, inputEvent);
    }
}

void EpollAdpt::modFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int inputEvent = pFdEvent->events();

    struct epoll_event event;
    bzero(&event, sizeof event);
    event.events = inputEvent;
    event.data.ptr = pFdEvent;
    
    if (::epoll_ctl(epollfd_, EPOLL_CTL_MOD, inputFd, &event) < 0)
    {
        LOG_FATAL("mod fd = %d events = %d fail", inputFd, inputEvent);
    }
    else
    {
        fdEventMap_[inputFd] = pFdEvent;
        LOG_TRACE("mod fd = %d; events = %d", inputFd, inputEvent);
    }
}

void EpollAdpt::delFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int inputEvent = pFdEvent->events();

    struct epoll_event event;
    bzero(&event, sizeof event);
    event.events = inputEvent;
    event.data.ptr = pFdEvent;

    fdEventMap_.erase(inputFd);
    
    if (::epoll_ctl(epollfd_, EPOLL_CTL_DEL, inputFd, &event) < 0)
    {
        LOG_ERROR("del fd = %d events = %d fail", inputFd, inputEvent);
    }
    else
    {
        LOG_TRACE("del fd = %d; events = %d", inputFd, inputEvent);
    }
}

