
#include "PollAdpt.h"
#include "FdEvent.h"
#include "Logging.h"

#include <assert.h>
#include <errno.h>
#include <poll.h>

using namespace muduo;

PollAdpt::PollAdpt(EventLoop* loop)
: Poller(loop)
{
}

PollAdpt::~PollAdpt()
{
}

///
/// 调用系统的poll函数，得到发生状态变化的描述符，
/// 并将对应的FdEvent对象保存到vector容器FdEventList中
/// 
Timestamp PollAdpt::polling(int timeoutMs, FdEventList* pActFdEvents)
{
    /// 
    /// poll函数返回值大于0，表示当前处于就绪状态的描述符数量
    ///
    int numEvents = ::poll(&*pollfds_.begin(), pollfds_.size(), timeoutMs);
    int savedErrno = errno;
    Timestamp now(Timestamp::now());
    if (numEvents > 0) 
    {
        LOG_TRACE("%d events happended", numEvents);
        fillActiveFdEvents(numEvents, pActFdEvents); // 得到当前发生状态变化的全部描述符
    }
    else if (numEvents == 0)
    {
        LOG_TRACE("nothing happended");
    }
    else
    {
        if (savedErrno != EINTR) // EINTR表示由于信号中断，没读到任何数据
        {
            errno = savedErrno;
            LOG_ERROR("polling errno=%", savedErrno);
        }
    }
    return now;
}

///
/// 遍历保存pollfd的vector容器，找出有活动事件的pollfd
///
void PollAdpt::fillActiveFdEvents(int numEvents, FdEventList* pActFdEvents) const
{
    for (PollFdList::const_iterator pfd = pollfds_.begin();
         pfd != pollfds_.end() && numEvents > 0; ++pfd)
    {
        /// pollfd结构体中的revents变量初始值为0，
        /// 发生状态变化时，poll函数会设置revents变量大于零
        if (pfd->revents <= 0)
        {
            continue;
        }
        
        FdEventMap::const_iterator it = fdEventMap_.find(pfd->fd);
        if (it == fdEventMap_.end())
        {
            assert(0);
            continue;
        }

        FdEvent* pFdEvent = it->second;
        if (pFdEvent->fd() != pfd->fd)
        {
            assert(0);
            continue;
        }
        
        pFdEvent->set_revents(pfd->revents);
        pActFdEvents->push_back(pFdEvent);
        
        --numEvents;
    }
}

void PollAdpt::addFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int inputEvent = pFdEvent->events();

    if (fdEventMap_.find(inputFd) == fdEventMap_.end())
    {
        LOG_TRACE("add fd = %d; events = %d", inputFd, inputEvent);
        
        struct pollfd pfd;
        pfd.fd = inputFd;
        pfd.events = static_cast<short>(inputEvent);
        pfd.revents = 0;
        pollfds_.push_back(pfd);
        
        int idx = static_cast<int>(pollfds_.size())-1;
        pFdEvent->set_index(idx);
        fdEventMap_[inputFd] = pFdEvent;
    }
    else
    {
        assert(0);
    }
}

void PollAdpt::modFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int fdIndex = pFdEvent->index();
    int inputEvent = pFdEvent->events();
    
    if ((fdEventMap_.find(inputFd) == fdEventMap_.end())
      ||(fdEventMap_[inputFd] != pFdEvent)
      ||(fdIndex < 0)
      ||(fdIndex >= static_cast<int>(pollfds_.size())))
    {
        assert(0);
        return;
    }
   
    struct pollfd& pfd = pollfds_[fdIndex];
    if((pfd.fd != inputFd) && (pfd.fd != -inputFd-1))
    {
        assert(0);
        return;
    }
    
    pfd.events = static_cast<short>(inputEvent);
    pfd.revents = 0;
    if (pFdEvent->isNoneEvent())
    {
        pfd.fd = -inputFd-1; // ignore this pollfd
    }
    else
    {
        pfd.fd = inputFd;
    }
    
    LOG_TRACE("mod fd = %d; events = %d", inputFd, inputEvent);
}

void PollAdpt::delFdEvent(FdEvent* pFdEvent)
{
    int inputFd = pFdEvent->fd();
    int fdIndex = pFdEvent->index();
    
    if ((fdEventMap_.find(inputFd) == fdEventMap_.end())
      ||(fdEventMap_[inputFd] != pFdEvent)
      ||(fdIndex < 0)
      ||(fdIndex >= static_cast<int>(pollfds_.size())))
    {
        assert(0);
        return;
    }

    const struct pollfd& pfd = pollfds_[fdIndex]; 
    if((pfd.fd != inputFd) && (pfd.fd != -inputFd-1))
    {
        assert(0);
        return;
    }
    
    fdEventMap_.erase(inputFd);
    
    if (size_t(fdIndex) == pollfds_.size()-1)
    {
        pollfds_.pop_back();
    }
    else
    {
        int lastFd = pollfds_.back().fd;
        iter_swap(pollfds_.begin()+fdIndex, pollfds_.end()-1);
        pollfds_.pop_back();
        
        if (lastFd < 0)
        {
            lastFd = -lastFd-1;
        }
        fdEventMap_[lastFd]->set_index(fdIndex);
    }

    LOG_TRACE("del fd = %d", inputFd);
}

