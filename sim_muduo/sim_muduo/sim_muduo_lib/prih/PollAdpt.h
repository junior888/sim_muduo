
#ifndef _MUDUO_POLL_WRAP_H_
#define _MUDUO_POLL_WRAP_H_

#include "Poller.h"
#include <vector>

struct pollfd;

namespace muduo
{

///
/// IO Multiplexing with poll(2).
///
class PollAdpt : public Poller
{
public:
    PollAdpt(EventLoop* loop);
    
    virtual ~PollAdpt();

    virtual Timestamp polling(int timeoutMs, FdEventList* pActFdEvents);
    
    virtual void addFdEvent(FdEvent* pFdEvent);

    virtual void modFdEvent(FdEvent* pFdEvent);

    virtual void delFdEvent(FdEvent* pFdEvent);


private:
    void fillActiveFdEvents(int numEvents, FdEventList* pActFdEvents) const;

    ///
    /// typedef struct pollfd {
    ///     int fd;         /* 需要检测的文件描述符             */
    ///     short events;   /* 对文件描述符fd上期望的事件       */
    ///     short revents;  /* 文件描述符fd上当前实际发生的事件 */
    /// } pollfd_t;
    /// 
    typedef std::vector<struct pollfd> PollFdList;
    PollFdList pollfds_;

    
};

}
#endif  // _MUDUO_POLL_H_

