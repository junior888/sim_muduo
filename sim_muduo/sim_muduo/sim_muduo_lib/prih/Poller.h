
#ifndef _MUDUO_POLLER_H_
#define _MUDUO_POLLER_H_

#include "copyable.h"
#include "Timestamp.h"

#include <map>
#include <vector>

namespace muduo
{
class FdEvent;
class EventLoop;

///
/// Base class for IO Multiplexing
///
/// This class doesn't own the Channel objects.
class Poller : public noncopyable
{
public:
   
    typedef std::vector<FdEvent*> FdEventList;
    
    Poller(EventLoop* loop)
    {
        ownerLoop_ = loop;
    }
    
    virtual ~Poller(){}

    static Poller* CreatePoller(EventLoop* loop, int PollType = 0);

    /// Polls the I/O events.
    /// Must be called in the loop thread.
    virtual Timestamp polling(int timeoutMs, FdEventList* pActFdEvents)= 0;

    /// add the interested I/O events.
    /// Must be called in the loop thread.
    virtual void addFdEvent(FdEvent* pFdEvent) = 0;

    /// modify the interested I/O events.
    /// Must be called in the loop thread.
    virtual void modFdEvent(FdEvent* pFdEvent) = 0;

    /// Remove the channel, when it destructs.
    /// Must be called in the loop thread.
    virtual void delFdEvent(FdEvent* pFdEvent) = 0;

protected:
    typedef std::map<int, FdEvent*> FdEventMap; 
    FdEventMap fdEventMap_;  // 记录全部被轮询的FdEvent对象

private:
    EventLoop* ownerLoop_;
};

}
#endif  // _MUDUO_POLLER_H_

