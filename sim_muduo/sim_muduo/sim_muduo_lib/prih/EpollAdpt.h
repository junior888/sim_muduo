
#ifndef _MUDUO_EPOLL_WRAP_H_
#define _MUDUO_EPOLL_WRAP_H_

#include "Poller.h"
#include "Logging.h"

#include <vector>
#include <sys/epoll.h>
#include <unistd.h> // close

struct epoll_event;

namespace muduo
{
///
/// IO Multiplexing with epoll(4).
///
class EpollAdpt : public Poller
{
public:
    EpollAdpt(EventLoop* loop);
    
    virtual ~EpollAdpt();

    virtual Timestamp polling(int timeoutMs, FdEventList* pActFdEvents);
    
    virtual void addFdEvent(FdEvent* pFdEvent);

    virtual void modFdEvent(FdEvent* pFdEvent);

    virtual void delFdEvent(FdEvent* pFdEvent);

private:
    static const int kInitEventListSize = 16;

    void fillActiveFdEvents(int numEvents, FdEventList* pActFdEvents) const;

    int epollfd_; // epoll������
    /// 
    /// struct epoll_event {
    ///     __uint32_t events;   /* Epoll events       */
    ///     epoll_data_t data;   /* User data variable */
    /// };
    /// 
    /// typedef union epoll_data {  
    ///     void *ptr;  
    ///     int fd;  
    ///     __uint32_t u32;  
    ///     __uint64_t u64;  
    /// } epoll_data_t;  
    /// 
    typedef std::vector<struct epoll_event> EventList;
    EventList events_;
};


}
#endif  // _MUDUO_EPOLL_WRAP_H_

