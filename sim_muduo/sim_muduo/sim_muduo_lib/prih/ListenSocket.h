#ifndef _MUDUO_ACCEPTOR_H_
#define _MUDUO_ACCEPTOR_H_

#include "copyable.h"
#include "Logging.h"
#include "FdEvent.h"
#include "InetAddress.h"
#include "ThreadUtil.h"

#include <map>
#include <fcntl.h>

namespace muduo
{

const int NoReusePort = 0;
const int ReusePort   = 1;

class TcpServer;

class IdleFd
{
public:

    IdleFd()
    : idleFd_(::open("/dev/null", O_RDONLY | O_CLOEXEC))
    {
        LOG_INFO("IdleFd =%d", idleFd_);
    }

    ~IdleFd()
    {
        ::close(idleFd_);
    }

    void useIdleFd(int listenFd)
    {
        MutexLocking lock(mutex_);
        ::close(idleFd_);
        idleFd_ = ::accept(listenFd, NULL, NULL);
        ::close(idleFd_);
        idleFd_ = ::open("/dev/null", O_RDONLY | O_CLOEXEC);
    }

private:

    MutexLock mutex_;
    
    int idleFd_;

};

///
/// ListenSocket of incoming TCP connections.
///
class ListenSocket : public FdEvent
{
public:
    ListenSocket(TcpServer* tcpServer, const InetAddress& listenAddr, bool isReusePort);

    virtual ~ListenSocket();
    
    void startListen();
    
    virtual void handleRead(Timestamp& receiveTime);
    
private:

    void setReuseAddr(bool on);
    
    void setReusePort(bool on);
    
    void bindAddress(const InetAddress& addr);

    int accept(InetAddress* peeraddr);
    
    TcpServer* tcpServer_;  // 指向所属的TcpServer对象

    static IdleFd s_IdleFd_;
};


}
#endif  // _MUDUO_ACCEPTOR_H_
