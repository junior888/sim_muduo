
#ifndef _MUDUO_CHANNEL_H_
#define _MUDUO_CHANNEL_H_

#include "copyable.h"
#include "Timestamp.h"

#include <functional>
#include <string>
#include <memory>
#include <assert.h>
#include <unistd.h>
#include <poll.h>



namespace muduo
{

const int NoneEvent  = 0;
const int ReadEvent  = POLLIN | POLLPRI;
const int WriteEvent = POLLOUT;

enum OptFlag{AddEvent, ModEvent, DelEvent};

class EventLoop;

class FdEvent : noncopyable
{
public:
    FdEvent(EventLoop* loop, int fd)
    : pEventLoop_(loop), 
      fd_(fd), 
      events_(0), 
      revents_(0), 
      index_(-1)
    {}
    
    virtual ~FdEvent()
    {}

    void enableReading(OptFlag optflag);
    
    void enableWriting(OptFlag optflag);
    
    void disableReading(OptFlag optflag);
    
    void disableWriting(OptFlag optflag);
    
    void disableAll(OptFlag optflag);

    void updateFdEvent(OptFlag optflag);
    
    // 虚函数需要继承类实现
    virtual void handleEvent(Timestamp& receiveTime);    

    virtual void handleRead(Timestamp& receiveTime){};
    
    virtual void handleWrite(){};
    
    virtual void handleClose(){};
    
    virtual void handleError(){};

    // 查询、修改类属性
    bool isNoneEvent() const { return events_ == NoneEvent; }
    bool isWriting()   const { return events_ &  WriteEvent;}
    bool isReading()   const { return events_ &  ReadEvent; }

    int fd() const { return fd_; }
    int events() const { return events_; }
    int revents() const { return revents_; }
    void set_revents(int revt) { revents_ = revt; }

    int index() { return index_; }
    void set_index(int idx) { index_ = idx; }
    
protected: // 受保护的数据只给继承类用

    EventLoop* pEventLoop_; // 负责处理本描述符的IO线程
    int fd_;                // 被监控的文件描述符
    int events_;            // 用户设置期望监听的事件
    int revents_;           // Poller返回实际监听得到的事件
    int index_;             // 记录FdEvent在vector中的位置used by Poll
};

}
#endif  // _MUDUO_CHANNEL_H_

