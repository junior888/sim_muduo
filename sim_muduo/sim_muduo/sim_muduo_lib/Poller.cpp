
#include "PollAdpt.h"
#include "EpollAdpt.h"
#include "EventLoop.h"

using namespace muduo;


Poller* Poller::CreatePoller(EventLoop* loop, int PollType)
{
    if (PollType == 0)
    {
       return new PollAdpt(loop);
    }
    else
    {
        return new EpollAdpt(loop);
    }
}


