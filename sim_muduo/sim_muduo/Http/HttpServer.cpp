
#include "HttpServer.h"
#include "HttpContext.h"
#include "HttpRequest.h"
#include "HttpResponse.h"

using namespace muduo;

namespace muduo
{

void defaultHttpCallback(const HttpRequest&, HttpResponse* resp)
{
    resp->setStatusCode(HttpResponse::k404NotFound);
    resp->setStatusMessage("Not Found");
    resp->setCloseConnection(true);
}


}

HttpServer::HttpServer(EventLoop* pMainLoop,
                       const InetAddress& listenAddr,
                       const std::string& name)
    : server_(threadNum, listenAddr, name, true, pMainLoop),
      httpCallback_(defaultHttpCallback)
{
    TcpServer::newConnectionCallback_ = std::bind(&HttpServer::newConnection, 
                                                 this, std::placeholders::_1);
                                         
    TcpServer::rcvMessageCallback_    = std::bind(&HttpServer::rcvMessage, 
                                                 this, 
                                                 std::placeholders::_1, 
                                                 std::placeholders::_2, 
                                                 std::placeholders::_3); 
                                                 
    TcpServer::delConnectionCallback_ = std::bind(&HttpServer::delConnection, 
                                                 this, std::placeholders::_1);

}

HttpServer::~HttpServer()
{
}

void HttpServer::start()
{
    server_.start();
}

void HttpServer::join()
{
    server_.joinThread();
}

void HttpServer::newConnection(const TcpLinkSPtr& conn)
{
    if (conn->connected())
    {
        HttpContext* pHttpContext = new HttpContext;
        conn->setContext((void *)pHttpContext);
    }
}

void HttpServer::delConnection(const muduo::TcpLinkSPtr& conn)
{
    HttpContext* pHttpContext = (HttpContext*)conn->getContext();
    if (pHttpContext != NULL)
    {
        delete pHttpContext;
    }
}

void HttpServer::rcvMessage(const TcpLinkSPtr& conn,
                            Buffer* buf, Timestamp receiveTime)
{
    HttpContext* context = (HttpContext*)conn->getContext();
    if (context == NULL)
    {
        return;
    }

    if (!context->parseRequest(buf, receiveTime))
    {
       conn->send("HTTP/1.1 400 Bad Request\r\n\r\n");
       conn->shutdown();
    }

    if (context->gotAll())
    {
        rcvRequest(conn, context->request());
        context->reset();
    }
}

void HttpServer::rcvRequest(const TcpLinkSPtr& conn, const HttpRequest& req)
{
    const std::string& connection = req.getHeader("Connection");
    bool close = ((connection == "close") ||
                  ((req.getVersion() == HttpRequest::kHttp10)
                  &&(connection != "Keep-Alive")));
                  
    HttpResponse response(close);
    httpCallback_(req, &response);
    Buffer buf;
    response.appendToBuffer(&buf);
    conn->send(&buf);
    if (response.closeConnection())
    {
        conn->shutdown();
    }
}

