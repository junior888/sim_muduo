#ifndef _MUDUO_HTTPSERVER_H_
#define _MUDUO_HTTPSERVER_H_

#include "copyable.h"
#include "TcpServer.h"

namespace muduo
{

class HttpRequest;
class HttpResponse;

class HttpServer : noncopyable
{
public:
    HttpServer(EventLoop* pMainLoop,
               const InetAddress& listenAddr,
               const std::string& name);
               
    ~HttpServer();
    
    void start();  // calls server_.start();

    void join();   // 如果threadNum>0，可调用此函数等待线程结束

    typedef std::function<void(const HttpRequest&, HttpResponse*)> HttpCallback;
    
    void setHttpCallback(const HttpCallback& cb)
    {
        httpCallback_ = cb;
    }

private:

    const int threadNum = 0; // 需要启动的监听线程数
    
    void newConnection(const muduo::TcpLinkSPtr& conn);

    void delConnection(const muduo::TcpLinkSPtr& conn);
    
    void rcvMessage(const TcpLinkSPtr& conn,
                    Buffer* buf, Timestamp time);

    void rcvRequest(const TcpLinkSPtr& conn, 
                    const HttpRequest& httpRequest);

    TcpServer server_;

    HttpCallback httpCallback_;
};
}

#endif //_MUDUO_HTTPSERVER_H_
