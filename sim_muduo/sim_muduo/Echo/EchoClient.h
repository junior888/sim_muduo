
#ifndef _MUDUO_ECHO_SERVER_H_
#define _MUDUO_ECHO_SERVER_H_

#include "TcpClient.h"
#include "InetAddress.h"
#include "TcpLink.h"
#include "Buffer.h"
#include "Timestamp.h"

class EchoClient;

class StdInFd : public muduo::FdEvent
{
public:

    StdInFd(EchoClient* pEchoClient);

    virtual ~StdInFd();
    
    virtual void handleRead(muduo::Timestamp& receiveTime);
    
private:

    EchoClient* pEchoClient_;
};

class EchoClient
{
public:

    EchoClient(muduo::EventLoop* pMainLoop, const muduo::InetAddress& serverAddr);
               
    void start();  // 

private:

    void newConnection(const muduo::TcpLinkSPtr& conn);

    void delConnection(const muduo::TcpLinkSPtr& conn);
    
    void rcvMessage(const muduo::TcpLinkSPtr& conn,
                    muduo::Buffer* buf, muduo::Timestamp time);

public:

    muduo::EventLoop* pMainLoop_;
    
    muduo::TcpClient tcpClient_;

    StdInFd stdIn_;

    std::weak_ptr<muduo::TcpLink> tcpLinkWPtr_;
};

#endif  // _MUDUO_ECHO_SERVER_H_
