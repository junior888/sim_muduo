
#include "EchoServer.h"
#include "Logging.h"
#include <unistd.h>

int main(int argc, char **argv)
{
    muduo::InetAddress listenAddr(2007);
    
    /*
    { // 方式一:多个Sever共用一个属于主线程的EventLoop
        muduo::EventLoop eventLoop;
        
        EchoServer echoServer(&eventLoop, listenAddr);
    
        echoServer.start();
    
        eventLoop.loop();
    }*/
    
    { 
        // 方式二:每个Sever内部启动属于自己的子线程
        //        主线程可调用server.jion等待子线程结束
        //        或调用server.detach分离子线程
        EchoServer echoServer(NULL, listenAddr);
        echoServer.start();
        echoServer.join();
    }
    
    return 0;
}

