
#ifndef _MUDUO_ECHO_SERVER_H_
#define _MUDUO_ECHO_SERVER_H_

#include "TcpServer.h"
#include "Buffer.h"
#include "InetAddress.h"
#include "TcpLink.h"
#include "Timestamp.h"


class EchoServer
{
public:
    EchoServer(muduo::EventLoop* pMainLoop, 
               const muduo::InetAddress& listenAddr);
               
    void start();  // calls server_.start();

    void join();   // 如果threadNum>0，可调用此函数等待线程结束
    
private:

    const int threadNum = 2; // 需要启动的监听线程数
    
    void newConnection(const muduo::TcpLinkSPtr& conn);

    void delConnection(const muduo::TcpLinkSPtr& conn);
    
    void rcvMessage(const muduo::TcpLinkSPtr& conn,
                    muduo::Buffer* buf, muduo::Timestamp time);

    muduo::TcpServer server_;
};

#endif  // _MUDUO_ECHO_SERVER_H_
