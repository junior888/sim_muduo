
#include "EchoClient.h"
#include "Logging.h"
#include <unistd.h>

int main(int argc, char **argv)
{
    if (argc != 3) {
        muduo::LOG_INFO("usage: EchoClient <IPaddress> <Port>");
        return -1;
    }
    
    int servPort = atoi(argv[2]);
    
    muduo::InetAddress serverAddr(argv[1], servPort);
    
    muduo::EventLoop mainLoop;
    
    EchoClient echoClient(&mainLoop, serverAddr);
    
    echoClient.start(); // ����echo�ͻ���
    
    mainLoop.loop();   // ����Reactor
    
    return 0;
}


